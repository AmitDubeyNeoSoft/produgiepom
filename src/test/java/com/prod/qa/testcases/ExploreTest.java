package com.prod.qa.testcases;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.prod.qa.base.TestBase;
import com.prod.qa.pages.ExplorePage;
import com.prod.qa.pages.GLAReport;
import com.prod.qa.pages.LoginPage;
import com.prod.qa.util.TestUtil;
//import com.qa.ExtentReportListener.ExtentReporterNG;



public class ExploreTest extends TestBase {

	LoginPage loginpage;
	ExplorePage explorepage;
	GLAReport glareport;
  String sheetName = "Sheet1";
	String userName;
	
	
	
	public ExploreTest(){
		super();
	}
	
//	   public ExploreTest(WebDriver chrome) {
//	        driver = chrome;
//	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//	    }
	
//	@Parameters("BrowserName")
	@BeforeTest()
	public void ClassObject() {
	
		String timeStamp = new SimpleDateFormat("yyyy.MM.dd_hh.mm.ss").format(Calendar.getInstance().getTime());
		
		//ExtentHtmlReporter reports = new ExtentHtmlReporter(".//target//Report.html");
		
		ExtentHtmlReporter reports = new ExtentHtmlReporter("./Reports/Report"+timeStamp+".html");
		reports.config().setDocumentTitle("Produgie Automation Report");
		reports.config().setReportName("Automation Test Result");
		reports.config().setTheme(Theme.DARK);
	    extend = new com.aventstack.extentreports.ExtentReports();
		extend.attachReporter(reports);
		initialization();
		loginpage = new LoginPage(driver);
		explorepage = new ExplorePage();
		glareport = new GLAReport();
		
		userName = prop.getProperty("username");
		// System.out.println("*********username******* "+userName );
		
		
	}
	@Test(priority = 1)
	public void LoginPage() {
		//loginpage.wipeout();
		//initialization();
		//Assert.assertEquals(loginpage.validateLoginPageTitle(), "Produgie");
		Assert.assertTrue(loginpage.validateProdugieLogo());
		System.out.println("Test 111111111111111111111111");
		
		loginpage.NonSSOLogin("jimmy.neouser3@yopmail.com", "P@ssw0rd123");
		//Assert.assertTrue(loginpage.validateForgotPassword());
	}
	
//	@Test(priority=1)
//	public void validateLoginPageTitle() {
//	
//		Assert.assertEquals(loginpage.validateLoginPageTitle(), "Produgie");
//		
//	}
//	
//	@Test(priority=2)
//	public void validateProdugieLogo() {
//		
//		loginpage.NonSSOLogin("neeti.mohan@yopmail.com", "P@ssw0rd123");
////		Assert.assertTrue(loginpage.validateProdugieLogo());
//}
//	
//	@Test(priority=3)
//	public void validateForgotPassword() {
//		Assert.assertTrue(loginpage.validateForgotPassword());
//	}
	
//	@DataProvider
//	public Object[][] getCRMTestData(){
//		
//		Object data[][] = TestUtil.getTestData(sheetName);
//		return data;
//	}
	
//	@Test(priority=2,dataProvider="getCRMTestData")
//	public void NonSSOLogin(String title, String firstName, String surname){
//		System.out.println("***************title****************"+title);
//		System.out.println("***************firstName****************"+firstName);
//		System.out.println("***************surname****************"+surname);
//		
//		System.out.println("***************surname****************"+surname);
//		loginpage.NonSSOLogin("klrahul.23@yopmail.com", "neosoft123");
//		//loginpage.wipeout();
//		
//		
//	}
	 @Test(priority=2)
	 public void ValidateAfterLoginPage() {
		 loginpage.wipeout();
		 loginpage.AfterLoginDashboardPage();
		 loginpage.SprintPage();
		 loginpage.DesignPage();
		 loginpage.ViewingRightsPage();
	 }
	 
	@Test(priority=3)
	public void GrowthLeaderAssessment() {
		
		explorepage.Exploree();
		explorepage.ValidationOnMyPortal();
		explorepage.RoleChallenges();
		explorepage.SelectRole();
		explorepage.ValidateStyleSurveyPage();
		explorepage.StyleSurvey();
		explorepage.ValidateStrategySurveyPage();
		explorepage.StrategySurvey();
		explorepage.ResearchQuestion();
		
	}
	@Test(priority=4)
	public void GLAReport() throws Exception {
		glareport.DashboardPage();
		
		
		glareport.MyGrowthLeaderProfile();
	//	glareport.GLAViewDetailedReport();
		//glareport.GLAPrintPreview();
		
	}
	
	 @AfterMethod
	 public void close(ITestResult result) throws IOException{
		 if(ITestResult.FAILURE==result.getStatus()) {
			 Screenshot(result.getMethod().getMethodName());
			 Markup m = MarkupHelper.createLabel("Test Case is Failed", ExtentColor.RED);
			// TestUtil.takeScreenshotAtEndOfTest1(result.getName());
			 ExtendReportFail(result.getMethod().getMethodName(), result.getName(), result.getThrowable().getMessage(), result.FAILURE);
		 }
		 if(ITestResult.SUCCESS==result.getStatus()) {
			 System.out.println("ELSEEEEEEEEEEEEEEEEEEEEEEEEE");
			 Markup m = MarkupHelper.createLabel("Test Case is Failed", ExtentColor.GREEN);
			 ExtendReportPass(result.getMethod().getMethodName(), result.getName(), result.getThrowable());
		 }
		
	 }
	 
	 @AfterTest
	 public void close() {
		
		
		 CloseReport();
		 //driver.quit();
	 }
	
}
