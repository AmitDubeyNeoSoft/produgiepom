package com.prod.qa.testcases;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Factory;

public class ProdugieLoadTesting {
	
	   @Factory
	    public Object[] createInstances() {

	        Object[] obj = new Object[5];

	        for (int iter = 0; iter < 5; iter++) {
	            obj[iter] = new ExploreTest();
	        }

	        return obj;
	    }

}
