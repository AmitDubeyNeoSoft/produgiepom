package com.prod.qa.pages;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.prod.qa.base.TestBase;

public class LoginPage extends TestBase{
	
	//Page Factory - OR:
	@FindBy(xpath="//img[@src='/assets/images/logo.svg']")
	WebElement ProdugieLogo;
	
	@FindBy(xpath="//span[text()='Forgot Password?']")
	WebElement ForgotPassword;
	
	@FindBy(xpath="//input[@type='email']")
	WebElement Email;
	
	@FindBy(xpath="//button[text()=' LOGIN WITH OKTA ']")
	WebElement LOGINWITHOKTA;
	
	@FindBy(id="okta-signin-username")
	WebElement OctaUserName;
	
	@FindBy(id="okta-signin-password")
	WebElement OctaPassword;
	
	@FindBy(id="okta-signin-submit")
	WebElement OctaSignIn;
	
	@FindBy(xpath="//span[text()='Configure Later']")
	WebElement ConfigureLater;
	
	@FindBy(xpath="//span[text()='use password instead?']")
	WebElement UsePasswordInstead;
	
	@FindBy(xpath="//input[@type='password']")
	WebElement Password;
	
	@FindBy(xpath="//button[text()=' LOGIN ']")
	WebElement Login;
	
	@FindBy(xpath="//div[text()='Please design your Sprint!']")
	WebElement PleasedesignyourSprint;
	
	@FindBy(xpath="//div[text()='Explore']")
	WebElement Explore;

	@FindBy(xpath="//div[text()='Trending in Produgie']")
	WebElement TrendinginProdugie;
	
	@FindBy(xpath="//div[text()='Design']")
	WebElement Design;
	
	@FindBy(xpath="//span[text()='Survey Report not generated yet!']")
	WebElement SurveyReportNotGeneratedYet;
	
	@FindBy(xpath="//div[text()='Sprints']")
	WebElement Sprints;
	
	@FindBy(xpath="//p[text()='Share your development plan to initiate your Sprint']")
	WebElement ShareYourDevelopmentPlan;
	
	@FindBy(xpath="//button[text()='Ok']")
	WebElement OKButton;
	
	@FindBy(xpath="//span[text()='arrow_drop_down']")
	WebElement SettingIcon;
	
	@FindBy(xpath="//button[text()='Viewing Rights']")
	WebElement ViewingRights;
	
	@FindBy(xpath="//span[text()='Please Design your Sprint first!']")
	WebElement PleaseDesignYourSprintFirst;
	
	@FindBy(xpath="//button[text()='Survey']")
	WebElement SurveyWipeout;
	
	@FindBy(xpath="//button[text()='Sprint']")
	WebElement SprintWipeout;
	
	@FindBy(xpath="//button[text()='Back']")
	WebElement BackWipeout;
	
//	@FindBy(name="username")
//	WebElement username;
//	
//	@FindBy(name="password")
//	WebElement password;
//	
//	@FindBy(xpath="//input[@type='submit']")
//	WebElement loginBtn;
//	
//	@FindBy(xpath="//button[contains(text(),'Sign Up')]")
//	WebElement signUpBtn;
//	
//	@FindBy(xpath="//img[contains(@class,'img-responsive')]")
//	WebElement crmLogo;
	
	//Initializing the Page Objects:
	
	public LoginPage(WebDriver driver){
		
		PageFactory.initElements(driver, this);
	}
	
	//Actions:
	public String validateLoginPageTitle(){
		return driver.getTitle();
	}
	
	public boolean validateProdugieLogo(){
		return ProdugieLogo.isDisplayed();
	}
	
	public boolean validateForgotPassword() {
		return ForgotPassword.isDisplayed();
	}
	
	public void NonSSOLogin( String Email_id,String password) {
		System.out.println("***************************"  + password); 
		//Email.clear();sleep(4000);
	//	Email.sendKeys(Email_id);
		Email.sendKeys(Email_id);
		LOGINWITHOKTA.click();
		OctaUserName.clear();
		OctaUserName.sendKeys(Email_id);
		//driver.findElement(By.id("timepass")).click();
		OctaPassword.clear();
		OctaPassword.sendKeys(password);
		OctaSignIn.click();
		sleep(5000);
		ConfigureLater.click();
//		if(ConfigureLater.isDisplayed())
//		{
//			ConfigureLater.click();
//		}
		//UsePasswordInstead.click();
//		Password.sendKeys(password);
//		Login.click();
		
	}
	
	public void AfterLoginDashboardPage() {
		String a = PleasedesignyourSprint.getText();
		boolean b = Explore.isDisplayed();
		boolean c = TrendinginProdugie.isDisplayed();
		
		assertEquals(a, "Please design your Sprint!");
		assertEquals(b, true);
		assertEquals(c, true);
		
	}
	
	public void DesignPage() {
		Design.click();
		sleep(2000);
		String a = SurveyReportNotGeneratedYet.getText();
		assertEquals(a, "Survey Report not generated yet!");
		
	}
	
	public void SprintPage()
	{
		Sprints.click();
		new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='Share your development plan to initiate your Sprint']"))).isDisplayed();
		String a = ShareYourDevelopmentPlan.getText();
		assertEquals(a,"Share your development plan to initiate your Sprint");
		OKButton.click();
	}
	
	public void ViewingRightsPage() {
		sleep(8000);
		//new WebDriverWait(driver, 60).until(ExpectedConditions.
		//new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='arrow_drop_down']"))).isDisplayed();
		SettingIcon.click();
		ActionMouseHover(SettingIcon,ViewingRights);
		new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[text()='Please Design your Sprint first!']"))).isDisplayed();
	    String a = PleaseDesignYourSprintFirst.getText();
	    sleep(2000);
		assertEquals(a,"Please Design your Sprint first!");
		}
	
	public void wipeout() {
		driver.get("https://my.produgie.com/wipeout");
		sleep(2000);
		SurveyWipeout.click();
		sleep(2000);
		SprintWipeout.click();
		sleep(2000);
		BackWipeout.click();
		sleep(8000);
		ProdugieLogo.click();
	}
	
//	public HomePage login(String un, String pwd){
//		username.sendKeys(un);
//		password.sendKeys(pwd);
//		//loginBtn.click();
//		    	JavascriptExecutor js = (JavascriptExecutor)driver;
//		    	js.executeScript("arguments[0].click();", loginBtn);
//		    	
//		return new HomePage();
//	}
	
}
