package com.prod.qa.pages;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.prod.qa.base.TestBase;

import org.openqa.selenium.support.PageFactory;

public class ExplorePage extends TestBase {
	boolean  flag = true;
	
	@FindBy(xpath="//div[text()=' Explore']")
	WebElement Explore;
	
	@FindBy(xpath="//div[text()='Design']")
	WebElement Design;
	
	@FindBy(xpath="//div[text()=' My Portal ']")
	WebElement MyPortal;
	
	@FindBy(xpath="//span[text()='Growth Leader Assessment']")
	WebElement GrowthLeaderAssessment;
	
	@FindBy(xpath="//span[text()='Growth Leader 360']")
	WebElement GrowthLeader360;
	
	@FindBy(xpath="//span[text()='GLA Report']")
	WebElement GLAReport;
	
	@FindBy(xpath="//span[text()='This will take 15 days']")
	WebElement ThisWillTake15Days;
	
	@FindBy(xpath="//button[text()=' Incomplete ']")
	WebElement RoleIncomplete;
	
	@FindBy(xpath="//span[text()='Select the top 3 key challenges in your Current Role']")
	WebElement TopKeyChallenges;
	
	@FindBy(xpath="//mat-radio-button[@id='mat-radio-2']")
	WebElement FirstRadioButton;
	
	@FindBy(xpath="//mat-radio-button[@id='mat-radio-4']")
	WebElement SecondRadioButton;
	
	@FindBy(xpath="//mat-radio-button[@id='mat-radio-6']")
	WebElement ThirdRadioButton;
	
	@FindBy(xpath="//button[text()=' Save ']")
	WebElement Save;
	
	@FindBy(xpath="//div[text()=' Completed ']")
	WebElement RoleCompleted;
	
	@FindBy(xpath="//div[text()=' Incomplete ']")
	WebElement SurveyIncomplete;
	
	@FindBy(xpath="//p[text()='Growth Leader Assessment']")
	WebElement GLA;
	
	@FindBy(xpath="//div[text()='Select Role']")
	WebElement SelectRoleButton;
	
	@FindBy(xpath="//p[text()=' Some statements vary by role. Please select your Current role level: ']")
	WebElement SelectRole;
	
	@FindBy(xpath="//p[text()='Individual Contributor']")
	WebElement IC;
	
	@FindBy(xpath="//p[text()='People Manager']")
	WebElement PM;
	
	@FindBy(xpath="//p[text()='Manager Of Managers']")
	WebElement MM;
	
	@FindBy(xpath="//p[text()='Function/Business Unit Manager']")
	WebElement BU;
	
	@FindBy(xpath="//p[text()='Chief Executive Officer(for SME/SMB only)']")
	WebElement SME;
	
	@FindBy(xpath="(//span[text()=' radio_button_unchecked '])[1]")
	WebElement ICRadioButton;
	
	@FindBy(xpath="//div[text()=' Save & Continue ']")
	WebElement SaveContinue;
		
	@FindBy(xpath="//p[text()=' Growth Leader Assessment : Style ']")
	WebElement GLAStyle;
	
	@FindBy(xpath="//p[text()='Strongly Disagree']")
	WebElement StronglyDisagree;
	
	@FindBy(xpath="//p[text()='Somewhat Disagree']")
	WebElement SomewhatDisagree;
	
	@FindBy(xpath="//p[text()='Neither Agree nor Disagree']")
	WebElement NeitherAgreeNorDisagree;
	
	@FindBy(xpath="//p[text()='Somewhat Agree']")
	WebElement SomewhatAgree;
	
	@FindBy(xpath="//p[text()='Strongly Agree']")
	WebElement StronglyAgree;
	
	@FindBy(xpath="//div[text()=' Continue ']")
	WebElement Continue;
	
	@FindBy(xpath="//button[text()='Cancel']")
	WebElement Cancel;
	
	@FindBy(xpath="//button[text()='Continue']")
	WebElement ContinueButton;
	
	@FindBy(xpath="//div[text()=' Save & Exit ']")
	WebElement SaveExit;
	
	@FindBy(xpath="//p[text()='You have completed the \"Style\" survey.']")
	WebElement StyleCompletedPopUp;
	
	@FindBy(xpath="//p[text()='Click on Next to complete the \"Strategy\" survey']")
	WebElement StrategyCompletedPopUp;
	
	@FindBy(xpath="//div[text()=' Next ']")
	WebElement NextButton;
	
	@FindBy(xpath="//p[text()=' Growth Leader Assessment : Strategy ']")
	WebElement GLAStrategy;
	
	@FindBy(xpath="//span[text()='Research Questions']")
	WebElement ResearchQuestionsText;
	
	@FindBy(xpath="//h5[text()='Please indicate your age (optional)']")
	WebElement AgeText;
	
	@FindBy(xpath="//h5[text()='What is your gender (optional)']")
	WebElement GenderText;
	
	@FindBy(xpath="//h5[text()=' What is the highest level of education you have completed?(optional) ']")
	WebElement EducationText;
	
	@FindBy(xpath="//h5[text()=' In which country do you live? (optional) ']")
	WebElement Country;
	
	@FindBy(id="age0")
	WebElement SelectAge;
	
	@FindBy(id="gender0")
	WebElement Gender;
	
	@FindBy(id="mat-select-0")
	WebElement EducationDropDown;
	
	@FindBy(xpath="//span[text()=' Bachelor’s Degree ']")
	WebElement BachelorsDegree;
	
	@FindBy(id="mat-select-2")
	WebElement CountryDropDown;
	
	@FindBy(xpath="//span[text()=' India​ ']")
	WebElement India;
	
	@FindBy(xpath="//button[text()=' Continue ']")
	WebElement ResearchContinueButton;
	
	@FindBy(xpath="//h5[text()=' Are you of Hispanic, Latino or of Spanish Origin? (optional) ']")
	WebElement Origin;
	
	@FindBy(xpath="(//input[@name='origin'])[2]")
	WebElement SelectOrigin;
	
	@FindBy(id="mat-select-value-5")
	WebElement CulturalGroup;
	
	@FindBy(xpath="//span[contains(text(),' Asian, Southern ')]")
	WebElement SelectCulturalGroup;
	
	@FindBy(name="ethnicalIdentificationOther")
	WebElement ethnicalIdentificationOther ;
	
	
	
	public ExplorePage(){
		PageFactory.initElements(driver, this);
	}

	public void Exploree() {
		sleep(10000);
		//Design.click();
		Explore.click();
		//waitForVisibility(MyPortal);
		new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()=' My Portal ']"))).isDisplayed();
		
	}
	
	public void ValidationOnMyPortal() {
		String a = MyPortal.getText();
        assertEquals(a, "My Portal");
        String b = GrowthLeaderAssessment.getText();
        assertEquals(b,"Growth Leader Assessment");
        String c = GrowthLeader360.getText();
        assertEquals(c,"Growth Leader 360");
        String d = GLAReport.getText();
        assertEquals(d,"GLA Report");
        String e = ThisWillTake15Days.getText();
        assertEquals(e,"This will take 15 days");
	}
	
	public void RoleChallenges() {
		sleep(2000);
		RoleIncomplete.click();
		new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Select the top 3 key challenges in your Current Role']"))).isDisplayed();
		String a= TopKeyChallenges.getText();
		assertEquals(a,"Select the top 3 key challenges in your Current Role");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		FirstRadioButton.click();
		SecondRadioButton.click();
		ThirdRadioButton.click();
	System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
	sleep(5000);
		Save.click();
		System.out.println("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
		new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()=' Completed ']"))).isDisplayed();
		System.out.println("cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc");
		String b= RoleCompleted.getText();
		System.out.println("ddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd");
		assertEquals(b,"Completed");
		System.out.println("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
		
		
		
	}
	
	public void SelectRole() {
		System.out.println("ffffffffffffffffffffffffffffffffffffff");
		sleep(5000);
		SurveyIncomplete.click();
		System.out.println("gggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg");
		new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='Growth Leader Assessment']"))).isDisplayed();
		String a= GLA.getText();
		assertEquals(a,"Growth Leader Assessment");
		SelectRoleButton.click();
		
		new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()=' Some statements vary by role. Please select your Current role level: ']"))).isDisplayed();
		String b= SelectRole.getText();
		assertEquals(b,"Some statements vary by role. Please select your Current role level:");
		String c= IC.getText();
		assertEquals(c,"Individual Contributor");
		String d= PM.getText();
		assertEquals(d,"People Manager");
		String e= MM.getText();
		assertEquals(e,"Manager Of Managers");
		String f= BU.getText();
		assertEquals(f,"Function/Business Unit Manager");
//		String g= SME.getText();
//		assertEquals(g,"Chief Executive Officer(for SME/SMB only)");
//		
		ICRadioButton.click();
		SaveContinue.click();
	
	}
	
	public void ValidateStyleSurveyPage() {
		
		new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()=' Growth Leader Assessment : Style ']"))).isDisplayed();
		
		assertEquals(GLAStyle.getText(),"Growth Leader Assessment : Style");
		assertEquals(StronglyDisagree.getText(),"Strongly Disagree");
		assertEquals(SomewhatDisagree.getText(),"Somewhat Disagree");
		assertEquals(NeitherAgreeNorDisagree.getText(),"Neither Agree nor Disagree");
		assertEquals(SomewhatAgree.getText(),"Somewhat Agree");
		assertEquals(StronglyAgree.getText(),"Strongly Agree");
		
	}
	
	public void StyleSurvey() {
		
		for(int i=1;i<=7;i++) {
			
		
		for(int j =1; j<=20; j++)
		{
			if(i==2 && j==5) {
				Continue.click();
				sleep(5000);
				Cancel.click();
			}
			if(i==3 && j==5) {
				Continue.click();
				sleep(5000);
				ContinueButton.click();
				sleep(8000);
				flag = false;
				//break;
			}
			
			if(!flag)
			{
				
				break;
			}
			sleep(500);
			new WebDriverWait(driver, 60).until(ExpectedConditions.elementToBeClickable(By.xpath("(//mat-radio-button[@value='Somewhat disagree'])["+j+"]"))).click();
			}
		if(flag)
		{   
			Continue.click();
			sleep(8000);
		}
		else {
			System.out.println("*****************************************************************");
			flag =true;
		}
		}
		SaveExit.click();
		sleep(5000);
		SurveyIncomplete.click();
		sleep(8000);
		for(int i=1;i<=8;i++) {
		
			if(i==4) {
				ContinueButton.click();
				sleep(8000);
			}
			if (i==8) {
				for(int j=1; j<=8; j++) {
					sleep(500);
					new WebDriverWait(driver, 60).until(ExpectedConditions.elementToBeClickable(By.xpath("(//mat-radio-button[@value='Somewhat disagree'])["+j+"]"))).click();
					
				}
				
			}
			
			Continue.click();
			sleep(8000);
		}
		
		
	}
	
    public void ValidateStrategySurveyPage() {
	     assertEquals(NextButton.getText(),"Next");
         NextButton.click();
	   
		new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()=' Growth Leader Assessment : Strategy ']"))).isDisplayed();
		
		assertEquals(GLAStrategy.getText(),"Growth Leader Assessment : Strategy");
		assertEquals(StronglyDisagree.getText(),"Strongly Disagree");
		assertEquals(SomewhatDisagree.getText(),"Somewhat Disagree");
		assertEquals(NeitherAgreeNorDisagree.getText(),"Neither Agree nor Disagree");
		assertEquals(SomewhatAgree.getText(),"Somewhat Agree");
		assertEquals(StronglyAgree.getText(),"Strongly Agree");
		
	}
	
	public void StrategySurvey() {
		
		for(int i=1;i<=5;i++) {
			
			
		for(int j =1; j<=20; j++)
		{
			if(i==2 && j==5) {
				Continue.click();
				sleep(5000);
				Cancel.click();
			}
			if(i==3 && j==5) {
				Continue.click();
				sleep(5000);
				ContinueButton.click();
				sleep(8000);
				flag = false;
				//break;
			}
			
			if(!flag)
			{
				
				break;
			}
			sleep(500);
			new WebDriverWait(driver, 60).until(ExpectedConditions.elementToBeClickable(By.xpath("(//mat-radio-button[@value='Somewhat disagree'])["+j+"]"))).click();
		}
		if(flag)
		{   
			Continue.click();
			sleep(8000);
		}
		else {
			System.out.println("*****************************************************************");
			flag =true;
		}
		}
		
		SaveExit.click();
		sleep(5000);
		SurveyIncomplete.click();
		sleep(8000);
		for(int i=1;i<=6;i++) {
		
			if(i==4) {
				ContinueButton.click();
				sleep(8000);
			}
			if (i==6) {
				for(int j=1; j<=6; j++) {
					sleep(500);
					new WebDriverWait(driver, 60).until(ExpectedConditions.elementToBeClickable(By.xpath("(//mat-radio-button[@value='Somewhat disagree'])["+j+"]"))).click();
					
				}
				
			}
			
			Continue.click();
			sleep(8000);
		}
		
	}
	
	public void ResearchQuestion() {
		 new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[text()='Research Questions']"))).isDisplayed();
			
		
		 Assert.assertEquals(ResearchQuestionsText.getText(), "Research Questions", "Research Questions Text not found!");
		 Assert.assertEquals(AgeText.getText(), "Please indicate your age (optional)", "Please indicate your age (optional) Text not found!");
		 Assert.assertEquals(GenderText.getText(), "What is your gender (optional)", "What is your gender (optional) Text not found!");
		 Assert.assertEquals(EducationText.getText(), "What is the highest level of education you have completed?(optional)", "What is the highest level of education you have completed?(optional) Text not found!");
		 Assert.assertEquals(Country.getText(), "In which country do you live? (optional)", "In which country do you live? (optional)");
			
		 SelectAge.click();
		 Gender.click();
		 EducationDropDown.click();
		 BachelorsDegree.click();
		 CountryDropDown.click();
		 India.click();
		 sleep(5000);
		 ResearchContinueButton.click();
		 
		 new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h5[text()=' Are you of Hispanic, Latino or of Spanish Origin? (optional) ']"))).isDisplayed();
		 Assert.assertEquals(Origin.getText(), "Are you of Hispanic, Latino or of Spanish Origin? (optional)", "Are you of Hispanic, Latino or of Spanish Origin? (optional)");
			
		 SelectOrigin.click();
		 CulturalGroup.click();
		 SelectCulturalGroup.click();
		 ethnicalIdentificationOther.sendKeys("Test");
		 ResearchContinueButton.click();
		 sleep(5000);
		
	}

}
