package com.prod.qa.pages;

import java.io.File;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.prod.qa.base.TestBase;

public class GLAReport  extends TestBase{
	
	@FindBy(xpath="(//div[text()=' Completed '])[2]")
	WebElement Surveyreport;
	
	@FindBy(xpath="//span[text()='Your Profile is ready!']")
	WebElement YourProfileIsReady;
	
	@FindBy(xpath="(//button[text()=' View your results '])[1]")
	WebElement ViewYourResults;
	
	@FindBy(xpath="//a[text()='Next']")
	WebElement Next;
	
	@FindBy(xpath="//span[contains(text(),'Use the filter to explore')]")
	//Use the filter to explore your results. The recommendations are based on your profile and current job challenges.
	WebElement FilterAppcue;
	
	@FindBy(xpath="//span[contains(text(),'Sign up for your')]")
	//Sign up for your Insights into Impact session. A facilitator will help you and other Users learn more about your results and designing a Sprint.
	WebElement ScheduleInsightAppCue;
	
	@FindBy(xpath="//a[text()='Close']")
	WebElement Close;
	
	@FindBy(xpath="//div[text()='Growth Leader Assessment']")
	WebElement GrowthLeaderAssessmentText;
	
	@FindBy(xpath="//p[text()='Filter']")
	WebElement Filter;
	
	@FindBy(id="settings")
	WebElement  StructureExecuteGrowthPlans;
	
	@FindBy(xpath="//span[text()=' Structure & Execute Growth Plans ']")
	WebElement  StructureExecuteGrowthPlansText;
	
	@FindBy(xpath="//span[text()='Style']")
	WebElement StyleText;
	
	@FindBy(xpath="//span[text()='Strategy']")
	WebElement StrategyText;
	
	@FindBy(xpath="//span[text()='View Detailed Report']")
	WebElement ViewDetailedReport;
	
	@FindBy(xpath="(//span[text()='Growth Leader Profile'])[1]")
	WebElement GrowthLeaderProfile;
	
	@FindBy(xpath="(//span[contains(text(),'Report for')])[1]")
	WebElement ReportFor;
	
	@FindBy(xpath="(//span[contains(text(),'2021 Organisation Solutions')])[2]")
	WebElement OSCopyRights;
	
	@FindBy(xpath="(//span[text()='Download'])[1]")
	WebElement ReportDownload;
	
	@FindBy(xpath="(//span[text()='Print'])[1]")
	WebElement ReportPrint;
	
	public GLAReport(){
		PageFactory.initElements(driver, this);
	}
	
	public void DashboardPage() {
		
		new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//div[text()=' Completed '])[2]"))).isDisplayed();
		 Assert.assertEquals(Surveyreport.getText(), "Completed", "Style and strategy survey is not completed");
		 Assert.assertEquals(YourProfileIsReady.getText(), "Your Profile is ready!", "Your Profile is ready! Text not Found");
		 
		 ViewYourResults.click();
			
	}
	
	public void MyGrowthLeaderProfile() {
		sleep(2000);
		new WebDriverWait(driver, 60).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//iframe[@role='dialog']")));
//		new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[text()='×']"))).isDisplayed();
//		driver.findElement(By.xpath("//a[text()='×']")).click();
		WebElement element = driver.findElement(By.xpath("//a[text()='×']"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		//driver.switchTo().defaultContent();
		sleep(5000);
//		new WebDriverWait(driver, 60).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//iframe[@title='Tooltip']")));
//		new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(text(),'Use the filter to explore')]"))).isDisplayed();
//		Next.click();
//		
//		driver.switchTo().defaultContent();
//		sleep(5000);
//		new WebDriverWait(driver, 60).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//iframe[@title='Tooltip']")));
//	    new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[contains(text(),'Sign up for your')]"))).isDisplayed();
//		Close.click();
		driver.switchTo().defaultContent();
		
		 Assert.assertEquals(GrowthLeaderAssessmentText.getText(), "Growth Leader Assessment", "Growth Leader Assessment Text not Found ");
		 Assert.assertEquals(Filter.getText(), "Filter", "Filter Text not Found");
		 
//		 StructureExecuteGrowthPlans.click();
//		 new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[text()=' Structure & Execute Growth Plans ']"))).isDisplayed();
//		 Assert.assertEquals(StructureExecuteGrowthPlansText.getText(), "Structure & Execute Growth Plans", "Structure & Execute Growth Plans Text not Found");
//		 Assert.assertEquals(StyleText.getText(), "Style", "Style Text not Found");
//		 Assert.assertEquals(StrategyText.getText(), "Strategy", "Strategy Text not Found");	
//		 sleep(5000);
//		 driver.get("https://dev.produgie.com/explore/aboutgrowthleaders");
		 
//		 ViewDetailedReport.click();
//		 new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//span[text()='Growth Leader Profile'])[1]"))).isDisplayed();
//		 Assert.assertEquals(GrowthLeaderProfile.getText(), "Growth Leader Profile", "Growth Leader Profile Text not Found");
//		 Assert.assertEquals(ReportFor.isDisplayed(), true);
//		// ScrollDown(3);
//		 
//		 Scroll(OSCopyRights);
////		 JavascriptExecutor js = (JavascriptExecutor) driver;
////		 WebElement Element = driver.findElement(By.xpath("(//span[contains(text(),'2021 Organisation Solutions')])[2]"));
////		 WebElement Element1 = driver.findElement(By.xpath("(//span[text()='Download'])[1]"));
////        js.executeScript("arguments[0].scrollIntoView();", Element);
//        sleep(3000);
//       // js.executeScript("arguments[0].scrollIntoView();", Element1);
//        
//        Scroll(ReportDownload);
//        sleep(3000);
//        ReportDownload.click();
//        isFileDownloaded();
		}

	public void GLAViewDetailedReport() throws Exception {
		
		 
		 ViewDetailedReport.click();
		 new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//span[text()='Growth Leader Profile'])[1]"))).isDisplayed();
		 Assert.assertEquals(GrowthLeaderProfile.getText(), "Growth Leader Profile", "Growth Leader Profile Text not Found");
		 Assert.assertEquals(ReportFor.isDisplayed(), true);
		 
		 Scroll(OSCopyRights);

       sleep(3000);
      
       Scroll(ReportDownload);
       sleep(3000);
//       JavascriptExecutor js = (JavascriptExecutor)driver;
//       WebElement button =driver.findElement(By.xpath("(//span[text()='Download'])[1]"));
//       js.executeScript("arguments[0].click();", button);
      ReportDownload.click();
      
       
     //  isFileDownloaded();
		
	}
	
	public void GLAPrintPreview() {
		sleep(5000);
		System.out.println("*************GLAPrintPreview start****************");
		String parent=driver.getWindowHandle();
		ReportPrint.click();
		sleep(5000);
		Set<String> windowHandles = driver.getWindowHandles();
		if (!windowHandles.isEmpty()) {
		    driver.switchTo().window((String) windowHandles.toArray()[windowHandles.size() - 1]);
		    System.out.println("IFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
		}
		//Now work with the dialog as with an ordinary page:  
		//driver.findElements(By.className("action-button")).get(1).click();
		driver.close();
		driver.switchTo().window(parent);
		driver.findElement(By.xpath("(//mat-icon[text()='close'])[1]")).click();
	
		
	}
}
