package com.prod.qa.pages;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.prod.qa.base.TestBase;

public class TEST extends TestBase {
	public static boolean isFileDownloaded1(String downloadPath, String fileName, File NewFileName) {
		  File dir = new File(downloadPath);
		  File[] dirContents = dir.listFiles();

		  for (int i = 0; i < dirContents.length; i++) {
		      if (dirContents[i].getName().equals(fileName)) {
		          // File has been found, it can now be deleted:
		       //   dirContents[i].delete();
		          dirContents[i].renameTo(NewFileName);
		          System.out.println("DELETED");
		          return true;
		      }
		          }
		      return false;
		  }
	
	
	public static boolean isFileDownloaded(String filePath, File NewFileName) throws Exception {
	    final int SLEEP_TIME_MILLIS = 1000;
	    File file = new File(filePath);
	    final int timeout = 60* SLEEP_TIME_MILLIS;
	    int timeElapsed = 0;
	    while (timeElapsed<timeout){
	        if (file.exists()) {
	        	file.renameTo(NewFileName);
	            //System.out.println(fileName + " is present");
	            return true;
	        } else {
	            timeElapsed +=SLEEP_TIME_MILLIS;
	            Thread.sleep(SLEEP_TIME_MILLIS);
	        }
	    }
	    return false;
	}
	

	public static void main(String[] args) throws Exception {
		String timeStamp = new SimpleDateFormat("yyyy.MM.dd_hh.mm.ss").format(Calendar.getInstance().getTime());
		File rename = new File("C:\\Users\\user\\Downloads\\Amit Dubey_report (22)"+timeStamp+".pdf");
		//isFileDownloaded1("C:\\Users\\user\\Downloads\\New folder","Amit Dubey_report (22).pdf", rename);
		System.out.println("******************");
		
		isFileDownloaded("C:\\Users\\user\\Downloads\\Amit Dubey_report (22).pdf",rename);
		
		
		 
	}

}
