package com.prod.qa.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.prod.qa.util.TestUtil;
import com.prod.qa.util.WebEventListener;

public class TestBase {
	
	//public ThreadLocal<WebDriver> driver = new ThreadLocal<>();
	public static WebDriver driver;
	public static Properties prop;
	public  static EventFiringWebDriver e_driver;
	public static WebEventListener eventListener;
	
	public static String ImgName;
	public static String Date;
	public static ExtentReports extend;
	
//	public void setDriver(WebDriver driver) {
//		this.driver.set(driver);
//	}
//	
//	public WebDriver getDriver() {
//		return this.driver.get();
//	}
	public TestBase(){
		try {
			prop = new Properties();
			FileInputStream ip = new FileInputStream(System.getProperty("user.dir")+ "/src/main/java/com/prod"
					+ "/qa/config/config.properties");
			prop.load(ip);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public  void initialization(){
	
	//public static void initialization(){
		String browserName = prop.getProperty("browser");
		//System.setProperty("webdriver.chrome.driver", "./WebDriver/chromedriver.exe");	
	//driver = new ChromeDriver();
//		if(browserName.equals("chrome")){
//			//if(BrowserName.equals("chrome")){
//		System.setProperty("webdriver.chrome.driver", "./WebDriver/chromedriver.exe");	
//			//setDriver(new ChromeDriver());
//	///////////////////Headless Browser//////////////////////////////////		
			ChromeOptions o= new ChromeOptions();
			o.setHeadless(true);
			o.addArguments("--disable-dev-shm-usage"); 
		    driver = new ChromeDriver(o); 
//    //////////////////////////////////r//////////////////////////////////	
//		    
//			driver = new ChromeDriver();
//			
//		}
//		else if(browserName.equals("FF")){
//			//else if(BrowserName.equals("FF")){
//			System.setProperty("webdriver.gecko.driver", "/Users/naveenkhunteta/Documents/SeleniumServer/geckodriver");	
//			//setDriver(new FirefoxDriver());
//			driver = new FirefoxDriver(); 
//		}
		
		
//		e_driver = new EventFiringWebDriver(driver);
//		// Now create object of EventListerHandler to register it with EventFiringWebDriver
//		eventListener = new WebEventListener();
//		e_driver.register(eventListener);
//		driver = e_driver;
		
		 Dimension d = new Dimension(1440, 900);
		 driver.manage().window().setSize(d);
		
		//driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
		
		//driver.get(prop.getProperty("url"));
		driver.get("https://my.produgie.com/auth/verify-email");
		
		
//		getDriver().manage().window().maximize();
//		getDriver().manage().deleteAllCookies();
//		getDriver().manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
//		getDriver().manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
//		
//		getDriver().get(prop.getProperty("url"));
		
	}
	
	
	public String Screenshot(String ImgName) throws IOException {
	//public String Screenshot(String ImgName) throws IOException {
	
		//File SrcFile = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.FILE);
		File SrcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		System.out.println("IMG NAME SS "+ ImgName);
		
		String Destination = System.getProperty("user.dir")+"/screenshots/"+ImgName+".png";
		File FinalDestination = new File(Destination);
		FileUtils.copyFile(SrcFile, FinalDestination);
		return Destination;
		
	}
	public void ExtendReportPass(String TestName, String GetName, Throwable throwable) throws IOException {
		
		ExtentTest logger1 = extend.createTest(TestName);
		
		logger1.log(Status.PASS, " Test Case Passes is " +GetName);
		
		extend.flush();
	
	}
	
	public void ExtendReportFail(String TestName, String GetName, String Message, int failure) throws IOException {
		ExtentTest logger = extend.createTest(TestName);
		logger.log(Status.FAIL, " Test Case Failed is " +GetName);
		logger.log(Status.FAIL, " Test Case is Failed Due to - ' " + Message + "'");
		logger.log(Status.FAIL, " Exception " +failure);
		System.out.println("IMG NAME Report "+ ImgName);
		logger.addScreenCaptureFromPath(System.getProperty("user.dir")+"\\screenshots\\"+TestName+".png");
		extend.flush();
	
	}
	
	//////////////////////////////// Click /////////////////////////////////////////////////////////////////////////////////////////////
	
	public void ActionMouseHover(WebElement MainElement, WebElement SubElement) {
		//WebElement mainMenu = driver.findElement(By.xpath(MainElement));
        Actions actions = new Actions(driver);
        actions.moveToElement(MainElement);
       // WebElement subMenu = driver.findElement(By.xpath("//button[text()=' Viewing Rights ']"));
		actions.moveToElement(SubElement);
		actions.click().build().perform();
	}
	
	/////////////////////////////////////// wait //////////////////////////////////////////////////////////////////////////////////////////////
	
	public void sleep(long Time)
	{
		try {
			Thread.sleep(Time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void Scroll(WebElement Element) {
		 JavascriptExecutor js = (JavascriptExecutor) driver;
		// WebElement Element = driver.findElement(By.xpath("(//span[contains(text(),'2021 Organisation Solutions')])[2]"));
		js.executeScript("arguments[0].scrollIntoView();", Element);
		
	}
	
	public static boolean isFileDownloaded() throws Exception {
		String filePath = "C:\\Users\\user\\Downloads\\Neeti Mohan_report.pdf";
		String timeStamp = new SimpleDateFormat("yyyy.MM.dd_hh.mm.ss").format(Calendar.getInstance().getTime());
		File rename = new File("C:\\Users\\user\\Downloads\\Neeti Mohan_report"+timeStamp+".pdf");
		
	    final int SLEEP_TIME_MILLIS = 1000;
	    File file = new File(filePath);
	    final int timeout = 60* SLEEP_TIME_MILLIS;
	    int timeElapsed = 0;
	    while (timeElapsed<timeout){
	        if (file.exists()) {
	        	file.renameTo(rename);
	            System.out.println("Downloaded file is present");
	            return true;
	        } else {
	            timeElapsed +=SLEEP_TIME_MILLIS;
	            Thread.sleep(SLEEP_TIME_MILLIS);
	            System.out.println("Downloaded file is not present");
	        }
	    }
	    return false;
	}
	
    public boolean waitForVisibility(WebElement myPortal) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 60);
            wait.until(ExpectedConditions.visibilityOfElementLocated((By) myPortal));
            return true;
        } catch (TimeoutException e) {
            System.out.println("Element is not visible: " + myPortal);
            throw e;

        }
    }
	
	public void CloseReport() {
		extend.flush();
		extend.close();
	}
	
	
	

}
